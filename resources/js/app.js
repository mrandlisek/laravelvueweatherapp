require('./bootstrap');

window.Vue = require('vue').default;

import WeatherApp from "./components/WeatherApp";
const app = new Vue({
    components: {
        WeatherApp
    },
    el: '#app',
});
